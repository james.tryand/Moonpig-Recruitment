﻿namespace Moonpig.PostOffice.Interfaces
{
    using System;

    public interface IPostOffice
    {
        DateTime CalculateDespatchDate(Order order);
    }
}
