﻿using Moonpig.PostOffice.Data;
using Moonpig.PostOffice.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Moonpig.PostOffice.Tests
{
    public class PostOfficeV2Tests : PostOfficeTests
    {
        public override Func<IPostOffice> NewPostOffice
        {
            get
            {
                return () => new PostOfficeV2(() => new DbContext());
            }
        }
    }

    public class PostOfficeV2BaselineTests : PostOfficeBaselineTests
    {
        public override Func<IPostOffice> NewPostOffice
        {
            get
            {
                return () => new PostOfficeV2(() => new DbContext());
            }
        }
    }
}
