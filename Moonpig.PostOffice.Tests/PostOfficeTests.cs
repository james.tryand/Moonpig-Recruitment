﻿namespace Moonpig.PostOffice.Tests
{
    using System;
    using System.Collections.Generic;
    using Data;
    using Moonpig.PostOffice.Interfaces;
    using Shouldly;
    using Xunit;

    public class PostOfficeTests
    {
        public virtual Func<IPostOffice> NewPostOffice
        {
            get
            {
                return () => new PostOffice(() => new DbContext());
            }
        }

        [Fact]
        public void OneProductWithLeadTimeOfOneDay()
        {
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() {1}, new DateTime(2018, 1, 22)));
            date.Date.ShouldBe(new DateTime(2018, 1, 22).Date.AddDays(1));
        }

        [Fact]
        public void OneProductWithLeadTimeOfTwoDay()
        {
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 2 }, new DateTime(2018, 1, 22)));
            date.Date.ShouldBe(new DateTime(2018, 1, 22).Date.AddDays(2));
        }

        [Fact]
        public void OneProductWithLeadTimeOfThreeDay()
        {
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 3 }, new DateTime(2018, 1, 22)));
            date.Date.ShouldBe(new DateTime(2018, 1, 22).Date.AddDays(3));
        }

        [Fact]
        public void TwoProductsWithLeadTimeOfOneDay()
        {
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 1, 7 }, new DateTime(2018, 1, 22)));
            date.Date.ShouldBe(new DateTime(2018, 1, 22).Date.AddDays(1));
        }


        [Fact]
        public void OneProductWhereTheSupplierDoesntExist()
        {
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 8 }, new DateTime(2018, 1, 22)));
            date.Date.ShouldBe(new DateTime(2018, 1, 22).Date);
        }

        [Fact]
        public void TwoProductsWithMaxLeadTimeOfThirteenDays()
        {
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 3, 10 }, new DateTime(2018, 1, 23)));
            date.Date.ShouldBe(new DateTime(2018, 1, 23).Date.AddDays(13));
        }

        [Fact]
        public void TwoProductsWithMaxLeadTimeOfThirteenDaysInAdifferentOrder()
        {
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 10, 3 }, new DateTime(2018, 1, 23)));
            date.Date.ShouldBe(new DateTime(2018, 1, 23).Date.AddDays(13));
        }

        [Fact]
        public void MoonpigWeekendRecieptOnSaturdayHasExtraTwoDays()
        {
            // company 1 has a 1 day lead time.
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 1 }, new DateTime(2018,1,26)));
            date.Date.ShouldBe(new DateTime(2018, 1, 26).Date.AddDays(3));
        }

        [Fact]
        public void MoonpigWeekendRecieptOnSundayHasExtraDay()
        {
            // product 1 from supplier 4 has a 3 day lead time.
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 3 }, new DateTime(2018, 1, 25)));
            date.Date.ShouldBe(new DateTime(2018, 1, 25).Date.AddDays(4));
        }

        [Fact]
        public void SendingOnASaturdayAddsTwoAdditionalDaysToAThreeDayShipment()
        {
            // product 1 from supplier 4 has a 3 day lead time.
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 3 }, new DateTime(2018, 1, 27)));
            date.Date.ShouldBe(new DateTime(2018, 1, 27).Date.AddDays(5));
        }

        [Fact]
        public void SendingOnASundayAddsOneAdditionalDayToAThreeDayShipment()
        {
            // product 1 from supplier 4 has a 3 day lead time.
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 3 }, new DateTime(2018, 1, 27)));
            date.Date.ShouldBe(new DateTime(2018, 1, 28).Date.AddDays(4));
        }

        [Fact]
        public void SendingOnASaturdayAddsFourAdditionalDaysToASixDayShipment()
        {
            // product 9 from supplier 5 has a 6 day lead time.
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 9 }, new DateTime(2018, 1, 27)));
            date.Date.ShouldBe(new DateTime(2018, 1, 27).Date.AddDays(9));
        }

        [Fact]
        public void SendingOnASundayAddsThreeAdditionalDaysToASixDayShipment()
        {
            // product 9 from supplier 5 has a 6 day lead time.
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 9 }, new DateTime(2018, 1, 28)));
            date.Date.ShouldBe(new DateTime(2018, 1, 28).Date.AddDays(8));
        }
    }
}
