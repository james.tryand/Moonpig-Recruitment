﻿namespace Moonpig.PostOffice.Tests
{
    using System;
    using System.Collections.Generic;
    using Data;
    using Interfaces;
    using Shouldly;
    using Xunit;

    /**
     * The point of this suite of tests is to ensure that the given service/function behaves itself and responds and handles itself appropriately given something wrong.
     */
    public class PostOfficeBaselineTests
    {
        public virtual Func<IPostOffice> NewPostOffice
        {
            get
            {
                return () => new PostOffice(() => new DbContext());
            }
        }

        ///**
        // * This does not compile but might be provided to us. Only worth continuing if there is shown to be actual real life scenarios where this is called.
        // */
        //[Fact]
        //public void NullDateTimeShouldFail()
        //{
        //    DateTime invalidDate;
        //    IPostOffice postOffice = NewPostOffice();
        //    var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 1 }, invalidDate));
        //    date.Date.ShouldBe(DateTime.Now.Date.AddDays(1));
        //}

        /**
         * There is only a date that can be recieved.
         * using date handed as the appropriate return date.
         * Alternative options include using the time to process time; so request durations can be verified. 
         * although that really should be something provided in the http header responses. 
         * so just going for the simple option for now.
         */
        [Fact]
        public void EmptyProductsListShouldReturnSensibleResponse()
        {
            var now = DateTime.Now;
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { }, now));
            date.ShouldBe(now);
        }

        [Fact]
        public void NullProductsListShouldReturnSensibleResponse()
        {
            var now = DateTime.Now;
            List<int> nulllist = null;
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(nulllist, now));
            date.ShouldBe(now);
        }

        /**
         * There is a possiblity for some reason that the product might not be available for whatever reason.
         * If the product is now unknown there is no way to sensibly return a valid estimation on the date.
         * Once again the trusty 'orderDate' should be returned so that this can be gracefully handled?
         * (Although at this point, it's becoming clear that as this response is being used for a number of reasons,
         * the sensible and appropriate response is to provide/handle an appropriate error response).
         */
        [Fact]
        public void UnknownSingleProductListShouldReturnSensibleResponse()
        {
            var now = DateTime.Now;
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 999 }, now));
            date.ShouldBe(now);
        }

        /**
         * This also holds for when most of the order is good. Resubmitting without the offending item would be the best response.
         * Certainly hiding it wouldn't be good either.
         */
        [Fact]
        public void UnknownProductInListShouldReturnSensibleResponse()
        {
            var now = DateTime.Now;
            IPostOffice postOffice = NewPostOffice();
            var date = postOffice.CalculateDespatchDate(new Order(new List<int>() { 1, 2, 999 }, now));
            date.ShouldBe(now);
        }
    }
}
