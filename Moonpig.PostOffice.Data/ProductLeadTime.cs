﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Moonpig.PostOffice.Data
{
    /**
     * This flat denormalized structure would be a more appropriate structure to use to calculate lead times.
     * As it's only updated from changes to the 'domain' ( not given here ), it's a more performant option.
     */
    public class ProductLeadTime
    {
        public int ProductId { get; set; }

        public string Name { get; set; }

        public int SupplierId { get; set; }

        public int LeadTime { get; set; }
    }
}
