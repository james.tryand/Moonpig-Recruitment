﻿namespace Moonpig.PostOffice.Data
{
    using Moonpig.PostOffice.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PostOfficeV2 : IPostOffice
    {
        // as I'm copying this over, and just changing the mechanics i'm just going to copy aspects over as i need them.

        public PostOfficeV2(Func<IDbContext> dbContextBuilder)
        {
            DbContextBuilder = dbContextBuilder;
        }

        public Func<IDbContext> DbContextBuilder { get; }

        public DateTime CalculateDespatchDate(Order order)
        {
            // ensure that there's an order to verify.
            // this is different from the other responses as this one could well include the time.
            if (order.ProductIds is null || order.ProductIds.Count < 1)
            {
                return order.OrderDate;
            }

            var maxLeadTime = default(int);

            using (IDbContext dbContext = DbContextBuilder())
            {

                // this provides a performance hit, but ensures that orders with the issue that they cannot be found, raise the appropriate alert.
                // However this is a business decision, but the customer would not be alerted to the missing product, and the estimate would be
                // for only those products that do exist.
                // The performance hit in itself, would ensure that where there is a problem processing would stop sooner rather than later.
                var allProductsAvailable = order.ProductIds.All(productid => dbContext.ProductLeadTimes.Any(product => product.ProductId == productid));
                if (!allProductsAvailable)
                {
                    return order.OrderDate;
                }

                var leadTimes = order.ProductIds.SelectMany(
                        productId => dbContext.ProductLeadTimes.
                            Where(plt => plt.ProductId == productId).
                            Select(product => product.LeadTime)
                            );

                if (leadTimes.Any())
                {
                    maxLeadTime = leadTimes.Max();
                }
            }

            // the use of linq here is to defer the processing back to the source. This way we're not having to make multiple calls, and can 
            // lean on linq to ensure that only the final value is returned.

            // this works as the specification states the minimum is 1.
            // the intent is that if the lead time returned is the default, that means that the processing went awry, and thus
            // we don't have a valid lead time, and as such we should return the value handed to us.
            if (maxLeadTime is default(int))
            {
                return order.OrderDate;
            }

            var maxLeadTimeDate = WeekendExtensionFor(order.OrderDate).AddDays(maxLeadTime);
            return WeekendExtensionFor(maxLeadTimeDate);
        }

        private DateTime WeekendExtensionFor(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    return date.AddDays(2);
                case DayOfWeek.Sunday:
                    return date.AddDays(1);
                default:
                    return date.AddDays(0);
            }
        }
    }
}
