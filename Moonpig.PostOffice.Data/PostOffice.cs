﻿namespace Moonpig.PostOffice.Data
{
    using System;
    using System.Linq;
    using Interfaces;

    /**
     * Implementation of the postoffice interface for estimating dispatch dates.
     */
    public class PostOffice : IPostOffice
    {

        public PostOffice(Func<IDbContext> dbContextBuilder)
        {
            DbContextBuilder = dbContextBuilder;
        }

        public Func<IDbContext> DbContextBuilder { get; }

        /**
* This recieves an order and only determines the estimate for the dispatch date.
* This is a function of the lead times specific to the suppliers for the items given in the order.
* 
* This is composed of two aspects. 
* Primarily it is the slowest lead time of the given suppliers.
* Secondly the final lead time changes depending upon the return date for said supplier being during a weekend.
*/
        public DateTime CalculateDespatchDate(Order order)
        {
            // ensure that there's an order to verify.
            // this is different from the other responses as this one could well include the time.
            if (order.ProductIds is null || order.ProductIds.Count < 1)
            {
                return order.OrderDate;
            }

            var maxLeadTime = default(int);

            using (IDbContext dbContext = DbContextBuilder())
            {
                // this provides a performance hit, but ensures that orders with the issue that they cannot be found, raise the appropriate alert.
                // However this is a business decision, but the customer would not be alerted to the missing product, and the estimate would be
                // for only those products that do exist.
                // The performance hit in itself, would ensure that where there is a problem processing would stop sooner rather than later.
                var allProductsAvailable = order.ProductIds.All(productid => dbContext.Products.Any(product => product.ProductId == productid));
                if (!allProductsAvailable)
                {
                    return order.OrderDate;
                }

                // technically this could be a single linq statement, but this way it's more apparent what each SelectMany is doing
                var supplierIds = order.ProductIds.SelectMany(
                    productId => dbContext.Products.
                        Where(product => product.ProductId == productId).
                        Select(product => product.SupplierId)
                    );

                var leadTimes = supplierIds.SelectMany(
                    supplierid => dbContext.Suppliers.
                        Where(supplier => supplier.SupplierId == supplierid).Select(supplier => supplier.LeadTime)
                    );
                if(leadTimes.Any())
                {
                    maxLeadTime = leadTimes.Max();
                }
            }

            // the use of linq here is to defer the processing back to the source. This way we're not having to make multiple calls, and can 
            // lean on linq to ensure that only the final value is returned.

            // this works as the specification states the minimum is 1.
            // the intent is that if the lead time returned is the default, that means that the processing went awry, and thus
            // we don't have a valid lead time, and as such we should return the value handed to us.
            if (maxLeadTime is default(int)) 
            {
                return order.OrderDate;
            }

            var maxLeadTimeDate = WeekendExtensionFor(order.OrderDate).AddDays(maxLeadTime);
            return WeekendExtensionFor(maxLeadTimeDate);
        }

        private DateTime WeekendExtensionFor(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    return date.AddDays(2);
                case DayOfWeek.Sunday:
                    return date.AddDays(1);
                default:
                    return date.AddDays(0);
            }
        }
    }
}