﻿namespace Moonpig.PostOffice.Data
{
    using Moonpig.PostOffice.Interfaces;
    using System;
    using System.Linq;

    public interface IDbContext : IDisposable
    {
        IQueryable<Supplier> Suppliers { get; }

        IQueryable<Product> Products { get; }

        IQueryable<ProductLeadTime> ProductLeadTimes { get; }
    }
}
