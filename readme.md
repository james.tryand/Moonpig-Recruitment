# Moonpig Engineering recruitment test

We've not set a time limit, do whatever you feel is reasonable however consider
 this to be production quality code. 

When complete, please upload your solution and answers in a .zip to the google
drive link provided to you by the recruiter.

-----

**Parts 1 & 2 have already been completed** albeit lacking in quality. Please 
check the implementation of these and look at refactoring it into something 
that you consider to be clean and well tested.

Once done, extend your solution to capture the requirements listed in part 3.

Do not change the public interface `IPostOffice`. The provided DbContext
is a stubbed class which provides test data. Please feel free to use this
in your implementation and tests but do keep in mind that it would be 
switched for something like an EntityFramework DBContext backed by a 
real database in production.

Once you have completed the exercise please answer the questions listed below. 
We are not looking for essay length answers. You can add the answers in this 
document.

## Questions

Q1. What 'code smells' / anti-patterns did you find in the existing 
	implemention of part 1 & 2?

* dbcontext within the loop
* validation of product/suppliers provided
* trying not to explode / provide relevant feedback.
* validation of sanity of dates provided
* sensible naming of internal variables.
* explain intent in comments ( and/or include internal project reference )
* documentation on the interface
* documentation on the order object.
* mutable value for the calculated value.
* *noted multiple exit points can be seen as a code smell, but depends upon context. In this instance I would say likely not, as we're only dealing with a simple return object*
* inapproriate quality of tests - only testing for 'happy cases', not that the invalid cases fail.
* ... *(after refactoring)*
* The logic. This is making two requests, rather than leaning upon either linq, or the backend to return the approrpriate values.
* for something like this it should be optimised. naieve requests for a component such as this are inappropriate.
* rather than start with now, and then verify all lead times. Rather return largest lead time from options, and handle appropriately.
* the dbcontext isn't cleaned up afterwards despite using external resources.
* getters & setters on all properties
* not testing against the interface
* tests & harnesses could be clearer and more constrained to aid readability & testing
* the structures & dependencies needed fixing to ensure better adherence to solid principals.

Q2. What further steps would you take to improve the solution given more time?
* Essentially the 'domain' doesn't exist: this is more for how does the data get there ( including from a mass import from prior sources if necessary)
* Depending upon how it's used, and assuming for instance an sql backend, then using some optimized sql, probably through something like dapper. This could either be non-cached or using an SP.
* The work being done is during the request. it might be worth looking into having a service specific view of the data: such that a productid would include the suppliers lead time. the update of this would be when the lead time changes. this way the request is simplified to a single query of productid.
* Given more time the scope for this grows. As the actual dispatch times could be fed into a different interface which could then be used to influence decisions about supplier lead times.
* Internally it would be worth ensuring that http responses include service performance metrics. That would aid identification where bottlenecks exist. 
* if the service were to be more complex, it might be worth working with multiple http response types such as returning 202 with an expected response location and 302 once it's available. But naturally this is out of scope for this.
* Certainly one thing of note is the lifecycle of the service and its data. The assumption is that the data simply exists, in that it's assumed to be available from prior existing datasource. However that essentially hides the fact that this is coupled to a datasource that can now no longer be updated or refactored as 'another' service consumes it. The only plus here is that it does not alter the data, but simply read it. Nonetheless introducing a lifetime scope, and associated interface/functions would help ensure that the boundaries, and constraints for this are better managed and it becomes one less coupled liability in the company's technical assets.
* for the direction i was taking this - see the 'completerefactor' branch ( available from gitlab https://gitlab.com/james.tryand/Moonpig-Recruitment ). This was to build both the read model (this), writemodel (domain) and the infrastructure - following that have it portable to aws.

Q3. What's a technology that you're excited about and where do you see this 
    being applicable?
* At the moment, i'm excited about processes quality and culture, and how I can apply that. 
* Technologies that I'm excited about include Rx - which is an excellent way of helping to bring to the fore issues that are otherwise troublesome to deal with once they become apparent in production ( such as those raised by threading ). It also helps work become more clear and maintainable. 
* I'm also a proponent of the technical decisions that are collectively known as CQRS; these often circumvent difficult technical decisions, and help lead from one successful measure to the next. This can be applied throughout an organisation where reliability, visibility and integration are necessary.
* I'm also excited about machine learning techniques and how they can help automate otherwise difficult areas such as categorization and prediction. I am aware though that they're not yet to the point of being any kind of magic bullet, as a clear understanding of what needs to be achived.

Q4. What process would you take to identify a performance issue in a production
    environment? 
* Right now after having had a few days getting to grips with the AWS suite of products, I'd like to be able to apply those in real scenarios, as they have the potential to greatly simplify and ease issues.
* The first step would be to verify what's happening on live. Once requests are available, it's possible to provide benchmarking, and benchmarking through the pipeline to determine bottlenecks.
* Putting boundaries around a given service, for instance using XEvents to record requests and performance issues in live, would give feedback on rogue systems, and help aid the diagnosis.
* Replicating this on dev/uat would also be sensible, as this can help either rule out the code being an issue, and rather it being something else (such as a network problem), or help highlight was aspect needs fixing.

## NOTES

_Note: Changes in requirements happen in production as things are missed. As such I am including changes to specification and behaviour as is fit, with relevant comments in this document._

* Changed expectations, such that empty or invalid lists would always return the same time as the order time. (as no error response type is available on the interface (ie. a union type of error | EstimatedDispatchDate ))
* Altered dbContext to be IDisposable because it's intended to represent access to an external resource. 


## Programming Exercise - Moonpig Post Office

You have been tasked with creating a service that calculates the estimated 
despatch dates for a customer order. 

An order contains a collection of products that a customer has added to their 
shopping basket. Each of these products is supplied to Moonpig on demand by a number of 3rd party suppliers. Each supplier will start processing the request as soon as it's received. They have an agreed lead time in which to process the supplier order before delivering it to the Moonpig Post Office.

Once the Moonpig Post Office has received all products in an order it is 
despatched to the customer.  

Assumptions:

1. Suppliers start processing a supplier order on the same day that the order 		is received. For example, a supplier with a lead time of one day, receiving
	an order today will send it to Moonpig tomorrow.


2. For the purposes of this exercise we are ignoring time i.e. if a 
	supplier has a lead time of 1 day then an order received any time on 
	Tuesday would arrive at Moonpig on the Wednesday.

3. Once all products for an order have arrived at Moonpig from the suppliers, 
	they will be despatched to the customer on the same day.


### Part 1 

Write an implementation of `IPostOffice` that calculates the despatch date 
of an order. 


### Part 2

Moonpig Post Office staff are getting complaints from customers expecting 
packages to be delivered on the weekend. You find out that the Moonpig Post
Office is shut over the weekend. Packages received from a supplier on a weekend 
will be despatched the following Monday.

Modify the existing code to ensure that any orders received from a supplier
on the weekend are despatched on the following Monday.

### Part 3

The Moonpig Post Office is still getting complaints... It turns out suppliers 
don't work during the weekend as well, i.e. if an order is received on the 
Friday with a lead time of 2 days, Moonpig would receive and dispatch on the 
Tuesday.


Modify the existing code to ensure that any orders that would have been 
processed during the weekend resume processing on Monday.
